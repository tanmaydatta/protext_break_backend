# Readme

- This code is hosted on [heroku](http://protext-backend.herokuapp.com/). To make it work, install the chrome extension from [here](https://bitbucket.org/tanmaydatta/anti_protext_chrome_ext)
- It takes the fonts and the obfuscated text and returns the non-obfuscated text.
- It simply reads the fonts and create a mapping from the original text to the obfuscated text.
- After receiving the obfuscated text, it searches the map and returns the normal text.