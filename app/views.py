from app import app
from flask import request
import requests
from fontTools.ttLib import TTFont
from flask_cors import cross_origin

def decode(pro_a_name, pro_b_name, query):
    pro_a = TTFont(pro_a_name)["glyf"]
    cmap_a = TTFont(pro_a_name)["cmap"].tables[0].cmap
    cmap_a = dict((y,x) for x,y in cmap_a.items())
    pro_b = TTFont(pro_b_name)["glyf"]
    cmap_b = TTFont(pro_b_name)["cmap"].tables[0].cmap
    cmap_b = dict((y,x) for x,y in cmap_b.items())
    pro = TTFont("font.ttf")["glyf"]
    cmap = TTFont("font.ttf")["cmap"].tables[0].cmap
    cmap = dict((y,x) for x,y in cmap.items())

    mapping = {"a": {}, "b": {}, "pro": {}}

    for glyf in pro_a.keys():
        try:
            no = cmap_a[glyf]
            x = pro_a[glyf]
            mapping[no] = x.coordinates
        except:
            pass

    for glyf in pro_b.keys():
        try:
            no = cmap_b[glyf]
            x = pro_b[glyf]
            mapping[no] = x.coordinates
        except:
            pass

    for glyf in pro.keys():
        try:
            no = cmap[glyf]
            x = pro[glyf]
            mapping[str(x.coordinates)] = no
        except:
            pass

    eg = query
    ans = []
    for code in eg:
        try:
            code = ord(code)
            c = mapping[code]
            asc = mapping[str(c)]
            ans.append(chr(asc))
        except:
            ans.append(" ")

    return "".join(ans)


@app.route('/')
@cross_origin()
def index():
    base_url = "http://protext.hackerrank.com/"
    headers = {
    'upgrade-insecure-requests': "1",
    'x-devtools-emulate-network-conditions-client-id': "fb017d27-d8fe-4b69-ad9f-64257cd1d79a",
    'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
    'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    'accept-encoding': "gzip, deflate, sdch",
    'accept-language': "en-US,en;q=0.8,hi;q=0.6",
    'cookie': "hackerrank_mixpanel_token=9b292d8f-b145-443d-b45e-a80b0706b5d5; mp_36cfc98842f47eba17d79df294c189f0_mixpanel=%7B%22distinct_id%22%3A%20%2215aa79afd1b1e9-0f1dd82d013568-152b120c-1fa400-15aa79afd1e196%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fcodepair.hackerrank.com%2Fpaper%2Fgznjvcxxjlgkidbyjqkhcalmjluycnei%3Fb%3DeyJpbnRlcnZpZXdfaWQiOjMxNzY0NSwicm9sZSI6ImludGVydmlld2VyIiwic2hvcnRfdXJsIjoiaHR0cDovL2hyLmdzL2Q3OThjOCJ9%22%2C%22%24initial_referring_domain%22%3A%20%22codepair.hackerrank.com%22%7D; optimizelyEndUserId=oeu1488995395017r0.8789899881415029; hackerrankx_mixpanel_token=9b292d8f-b145-443d-b45e-a80b0706b5d5; _mkto_trk=id:487-WAY-049&token:_mch-hackerrank.com-1490295704426-12297; mp_dcd74fdb7c65d92ce5d036daddac0a25_mixpanel=%7B%22distinct_id%22%3A%20%229b292d8f-b145-443d-b45e-a80b0706b5d5%22%2C%22utm_source%22%3A%20%22linkedin.com%22%2C%22utm_medium%22%3A%20%22social%22%2C%22utm_campaign%22%3A%20%22buffer%22%2C%22utm_content%22%3A%20%22buffer0cd2f%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fwww.linkedin.com%2F%22%2C%22%24initial_referring_domain%22%3A%20%22www.linkedin.com%22%7D; _hp2_id.undefined=%7B%22userId%22%3A%223469636689849389%22%2C%22pageviewId%22%3A%222953315730758936%22%2C%22sessionId%22%3A%225434121479193385%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%223.0%22%7D; h_r=jobs; h_v=_default; _hp2_id.547804831=%7B%22userId%22%3A%228537031432246224%22%2C%22pageviewId%22%3A%223448003718247181%22%2C%22sessionId%22%3A%221776462258755629%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%223.0%22%7D; optimizelySegments=%7B%221709580323%22%3A%22false%22%2C%221717251348%22%3A%22gc%22%2C%221719390155%22%3A%22campaign%22%2C%222308790558%22%3A%22buffer%22%7D; optimizelyBuckets=%7B%7D; h_l=in_app; _biz_flagsA=%7B%22Version%22%3A1%2C%22Frm%22%3A%221%22%7D; _biz_uid=db30de300f0f42f7cf0e0bab39ef35c5; mp_bcb75af88bccc92724ac5fd79271e1ff_mixpanel=%7B%22distinct_id%22%3A%20%229b292d8f-b145-443d-b45e-a80b0706b5d5%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%2C%22%24search_engine%22%3A%20%22google%22%2C%22utm_source%22%3A%20%22facebook.com%22%2C%22utm_medium%22%3A%20%22social%22%2C%22utm_campaign%22%3A%20%22buffer%22%2C%22utm_content%22%3A%20%22buffer33545%22%7D; mp_86cf4681911d3ff600208fdc823c5ff5_mixpanel=%7B%22distinct_id%22%3A%20%2215a4357c695289-0258880eafc53d-152b120c-1fa400-15a4357c696705%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%2C%22%24search_engine%22%3A%20%22google%22%2C%22utm_source%22%3A%20%22facebook.com%22%2C%22utm_medium%22%3A%20%22social%22%2C%22utm_campaign%22%3A%20%22buffer%22%2C%22utm_content%22%3A%20%22buffer33545%22%7D; __utma=74197771.1764182959.1487188510.1497442839.1497704181.66; __utmz=74197771.1496044854.63.10.utmcsr=l.messenger.com|utmccn=(referral)|utmcmd=referral|utmcct=/; _biz_nA=218; _biz_pendingA=%5B%5D; _ga=GA1.2.1764182959.1487188510; _gid=GA1.2.1336002170.1497895105; _gat=1; X-VALID=TRUE",
    'cache-control': "no-cache",
    'postman-token': "9f8c749c-ee38-7c02-7ee0-283e9c21c1b8"
    }
    try:
        pro_a = base_url + request.args.get('pro_a').split('"')[1]
        pro_b = base_url + request.args.get('pro_b').split('"')[1]
        pro_a_name = request.args.get('pro_a').split('"')[1].split("/")[-1]
        pro_b_name = request.args.get('pro_b').split('"')[1].split("/")[-1]
        with open(pro_b_name, "wb") as f:
            f.write(requests.get(pro_b, headers=headers).content)
        with open(pro_a_name, "wb") as f:
            f.write(requests.get(pro_a, headers=headers).content)
        query = request.args.get('q')
        return decode(pro_a_name, pro_b_name, query)
    except Exception as e:
        return repr(e)
    return "Hello, World!"