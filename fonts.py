from fontTools.ttLib import TTFont
import sys


pro_a = TTFont(sys.argv[1])["glyf"]
cmap_a = TTFont(sys.argv[1])["cmap"].tables[0].cmap
cmap_a = dict((y,x) for x,y in cmap_a.items())
pro_b = TTFont(sys.argv[2])["glyf"]
cmap_b = TTFont(sys.argv[2])["cmap"].tables[0].cmap
cmap_b = dict((y,x) for x,y in cmap_b.items())
pro = TTFont(sys.argv[3])["glyf"]
cmap = TTFont(sys.argv[3])["cmap"].tables[0].cmap
cmap = dict((y,x) for x,y in cmap.items())

mapping = {"a": {}, "b": {}, "pro": {}}

for glyf in pro_a.keys():
	try:
		no = cmap_a[glyf]
		x = pro_a[glyf]
		mapping[no] = x.coordinates
	except:
		pass

for glyf in pro_b.keys():
	try:
		no = cmap_b[glyf]
		x = pro_b[glyf]
		mapping[no] = x.coordinates
	except:
		pass

for glyf in pro.keys():
	try:
		no = cmap[glyf]
		x = pro[glyf]
		mapping[str(x.coordinates)] = no
	except:
		pass

eg = sys.argv[4]
ans = []
for code in eg:
	try:
		code = ord(code)
		c = mapping[code]
		asc = mapping[str(c)]
		ans.append(chr(asc))
	except:
		ans.append(" ")

print("".join(ans))